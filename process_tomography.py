# Copyright 2021 Tamara Kohler, Emilio Onorati and Toby Cubitt

#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at

#        http://www.apache.org/licenses/LICENSE-2.0

#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.


import cirq 
import numpy as np
from cirq.ops import X
from cirq.ops import H
from cirq.ops import CNOT


#%%
def transfer_matrix(kraus_operators):
    
    # Function to compute the transfer matrix from the kraus operators using T = \sum K_i K_i^\dagger
    
    x = np.shape(kraus_operators)[0]
    y = np.shape(kraus_operators)[1]**2
    matrix_operator = np.zeros((y,y),dtype=complex)
    for i in range(x):
        matrix_operator += np.kron(kraus_operators[i,:,:],kraus_operators[i,:,:].conj())
    return matrix_operator

#%%
# Function to perform single qubit process tomography. Method and notation follows Nielsen & Chuang, page: 393

def single_qubit_process_tomography(qubits,test_circuit,sampler,repetitions=10000):
    
    # Compute the density matrix E(|0><0|) using state tomography
    circuit_zero = cirq.Circuit();
    circuit_zero.append(test_circuit);
    tomography_result_zero = cirq.experiments.single_qubit_state_tomography(sampler, qubits, circuit_zero, repetitions);
    rho_zero = tomography_result_zero.data;
    
    # Compute the density matrix E(|1><1|) using state tomography
    circuit_one = cirq.Circuit();
    circuit_one.append(X(qubits));
    circuit_one.append(test_circuit);
    tomography_result_one = cirq.experiments.single_qubit_state_tomography(sampler, qubits, circuit_one, repetitions);
    rho_one = tomography_result_one.data;
    
    # Compute the density matrix E(|+><+|) using state tomography
    circuit_plus = cirq.Circuit();
    circuit_plus.append(H(qubits));
    circuit_plus.append(test_circuit);
    tomography_result_plus = cirq.experiments.single_qubit_state_tomography(sampler, qubits, circuit_plus, repetitions);
    rho_plus = tomography_result_plus.data;
     
    # Compute the density matrix E(|-><-|) using state tomography       
    circuit_minus = cirq.Circuit();
    circuit_minus.append(X(qubits));
    circuit_minus.append(H(qubits));
    circuit_minus.append(cirq.S(qubits));
    circuit_minus.append(test_circuit);
    tomography_result_minus = cirq.experiments.single_qubit_state_tomography(sampler, qubits, circuit_minus, repetitions);
    rho_minus = tomography_result_minus.data;
    
    # Set up a numpy array containing the Kraus operator basis elements K'
    I = np.array([[1,0],[0,1]]);
    Xmatrix = np.array([[0,1],[1,0]]);
    Zmatrix = np.array([[1,0],[0,-1]]);
    Ymatrix = np.array([[0,-1j],[1j,0]]);
    tilde_kraus_operators = np.empty((4,2,2),dtype=complex);
    tilde_kraus_operators[0,:,:] = I;
    tilde_kraus_operators[1,:,:] = Xmatrix;
    tilde_kraus_operators[3,:,:] = Zmatrix;
    tilde_kraus_operators[2,:,:]  = Ymatrix;
    
    # Set up lambda matrix
    lambd = 0.5* np.block([[I,Xmatrix],[Xmatrix,-I]]);
    
    # Calculate the density matrixes E(|1><0|) and E(|0><1|)
    rho_two = rho_plus - (1j * rho_minus) -(0.5*(1-1j)*(rho_zero + rho_one));
    rho_three = rho_plus + (1j * rho_minus) -(0.5*(1+1j)*(rho_zero + rho_one));
    
    # Set up Chi matrix, and find U which diagonalises Chi
    Chi = lambd @ np.block([[rho_zero, rho_two],[rho_three, rho_one]]) @ lambd;
    [eigenvals,eigenvectors] = np.linalg.eig(Chi);
    U = eigenvectors;
    
    # Calculate Kraus operators via K_i = \sum_j d_i * U_(j,i) * K'_j
    kraus_operators = np.zeros((4,2,2),dtype=complex);
    for i in range(4):
        for j in range(4):
            kraus_operators[i,:,:] += (np.sqrt(eigenvals[i])*U[j,i]*tilde_kraus_operators[j,:,:]);
    
    # Calculate transfer matrix from Kraus operators
    matrix_rep = transfer_matrix(kraus_operators)
    return matrix_rep
    
    
#%%    
# Function to perform two qubit process tomography. Method and notation follows Nielsen & Chuang, pages: 390-392, specialised to two qubits

def two_qubit_process_tomography(qubits,test_circuit,sampler,repetitions = 100000):
    
    # Array which will contain 2-qubit Pauli group
    Etilde = np.empty([16,4,4],dtype=complex);
    
    # Define 1 qubit Pauli group and set up array containing G_1
    I = np.array([[1,0],[0,1]]);
    Xmatrix = np.array([[0,1],[1,0]]);
    Zmatrix = np.array([[1,0],[0,-1]]);
    Ymatrix = np.array([[0,-1j],[1j,0]]);
    Pauli = np.empty([4,2,2],dtype=complex);
    Pauli[0,:,:] = I
    Pauli[1,:,:] = Xmatrix
    Pauli[2,:,:] = Ymatrix
    Pauli[3,:,:] = Zmatrix 
    
    # Set up array containin G_2 
    for i in range(4):
        for j in range(4):
            k = ((4*i)+j)
            Etilde[k,:,:] = np.kron(Pauli[i,:,:],Pauli[j,:,:])
    
    # Set up array containing circuits to initialise qubits. 
    # Array elements [x,y] correspond to circuit to inialize to state |x> if x=y, to state |x>+|y> if y>x and to state |x>+i|y> if y<x. 
    # Where: x=0 -> |0,0>, x=1 -> |0,1>, x=2 -> |1,0>, x=3 -> |1,1>  
    initialization_circuits = np.empty((4,4),dtype=object)
    for i in range(4):
        for j in range(4):
            initialization_circuits[i,j] = cirq.Circuit()
            
    initialization_circuits[0,0].append([cirq.I(qubits[0]),cirq.I(qubits[1])])        
    initialization_circuits[0,1].append([H(qubits[1]),cirq.I(qubits[0])])
    initialization_circuits[0,2].append([H(qubits[0]),cirq.I(qubits[1])])
    initialization_circuits[0,3].append(H(qubits[0]))
    initialization_circuits[0,3].append(CNOT(qubits[0],qubits[1]))
    
    initialization_circuits[1,0].append([X(qubits[1]),H(qubits[1]),cirq.S(qubits[1]),cirq.I(qubits[0])])
    initialization_circuits[1,1].append([X(qubits[1]),cirq.I(qubits[0])])
    initialization_circuits[1,2].append([H(qubits[0]),X(qubits[1]),CNOT(qubits[0],qubits[1])])
    initialization_circuits[1,3].append([H(qubits[0]),X(qubits[1])])

    initialization_circuits[2,0].append([X(qubits[0]),H(qubits[0]),cirq.S(qubits[0]),cirq.I(qubits[1])])
    initialization_circuits[2,1].append([X(qubits[0]),H(qubits[0]),cirq.S(qubits[0]),X(qubits[1]),CNOT(qubits[0],qubits[1])])
    initialization_circuits[2,2].append([X(qubits[0]),cirq.I(qubits[1])])
    initialization_circuits[2,3].append([X(qubits[0]),H(qubits[1])])

    initialization_circuits[3,0].append([X(qubits[0]),H(qubits[0]),cirq.S(qubits[0]),CNOT(qubits[0],qubits[1])])
    initialization_circuits[3,1].append([X(qubits[0]),H(qubits[0]),cirq.S(qubits[0]),X(qubits[1])])
    initialization_circuits[3,2].append([X(qubits[1]),H(qubits[1]),cirq.S(qubits[1]),X(qubits[0])])
    initialization_circuits[3,3].append([X(qubits[0]),X(qubits[1])])
    
    # Use simulated state tomography to set up initial conditions
    tomography_results_initial = np.empty([4,4],dtype= object)
    rho = np.empty([4,4,4,4],dtype=complex)
    for i in range(4):
        for j in range(4):
            kraus_operators = np.asarray(cirq.channel(initialization_circuits[i,j]))
            transf = transfer_matrix(kraus_operators)
            tomography_results_initial[i,j] = transf@ np.array([[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]).T
                        
   
    # Calculate initial density matrices rho(n,m) = |n><m|
    for i in range(4):
        for j in range(4):
            if i==j:
                rho[i,j] = tomography_results_initial[i,j].reshape(4,4)
            elif i<j:
                rho[i,j] = (tomography_results_initial[i,j] + (1j*tomography_results_initial[j,i]) - (0.5*(1+1j)*(tomography_results_initial[i,i] + tomography_results_initial[j,j]))).reshape(4,4)
            elif i>j:
                rho[i,j] = (tomography_results_initial[j,i] - (1j*tomography_results_initial[i,j]) - (0.5*(1-1j)*(tomography_results_initial[i,i] + tomography_results_initial[j,j]))).reshape(4,4)

            
    # Calculate E\rhoE^\dagger and Beta arrays (where E are the Kraus basis elements)        
    ErhoE = np.empty([16,16,16,4,4],dtype=complex)
    Beta = np.empty([16,16,16,16],dtype=complex)
    rho_prime = np.reshape(rho,(16,16))
    for m in range(16):
        for n in range(16):
            for i in range(16):
                E_dagger = (Etilde[n,:,:]).conj().T
                ErhoE[m,n,i,:,:] = Etilde[m,:,:]@(np.reshape(rho,(16,4,4))[i,:,:])@E_dagger
                Beta[m,n,i,:] = np.linalg.tensorsolve(rho_prime,np.reshape(ErhoE,(16,16,16,16))[m,n,i,:])
    
    # Calculate kappa matrix from Beta using generalized inverse method                                                                                                    
    Beta_prime = np.reshape(Beta,(256,256))
    kappa_prime = np.linalg.pinv(Beta_prime)
    kappa = np.reshape(kappa_prime,[16,16,16,16])
    
    # Use `real' state tomography to construct final density matrices
    tomography_results_final = np.empty([4,4],dtype= object)
    for i in range(4):
        for j in range(4):
            initialization_circuits[i,j].append(test_circuit)
            tomography_results_final[i,j] = cirq.experiments.two_qubit_state_tomography(sampler,qubits[0],qubits[1],initialization_circuits[i,j],repetitions)
            
    # Calculate final density matrices: E_rho(n,m) = E(|n><m|)  
    E_rho = np.empty([4,4,4,4],dtype=complex)    
    for i in range(4):
        for j in range(4):
            if i==j:
                E_rho[i,j] = tomography_results_final[i,j].data
            elif i<j:
                E_rho[i,j] = tomography_results_final[i,j].data + (1j*tomography_results_final[j,i].data) - (0.5*(1+1j)*(tomography_results_final[i,i].data + tomography_results_final[j,j].data))
            elif i>j:
                E_rho[i,j] = tomography_results_final[j,i].data - (1j*tomography_results_final[i,j].data) - (0.5*(1-1j)*(tomography_results_final[i,i].data + tomography_results_final[j,j].data))
                                                 
    # Calculate lambda matrix using linalg solver                                                  
    lambd = np.empty([16,16],dtype=complex)
    E_rho_prime = np.reshape(E_rho,(16,16))
    for i in range(16):
        lambd[i,:] = np.linalg.tensorsolve(rho_prime,E_rho_prime[i,:])
    
    # Calculate Chi matrix from kappa, lambda matrices                                                                                                 
    Chi = np.zeros([16,16],dtype=complex)
    for m in range(16):
        for n in range(16):
            for i in range(16):
                for j in range(16):
                    Chi[m,n] += kappa[i,j,m,n]*lambd[i,j] 
                                                      
    # Find U which diagonalizse Chi                                                  
    [eigenvals,eigenvectors] = np.linalg.eig(Chi);
    U = eigenvectors; 
    
    # Calculate Kraus operators via K_i = \sum_j d_i * U_(j,i) * K'_j                                        
    kraus_operators = np.zeros((16,4,4),dtype=complex);
    for i in range(16):
        for j in range(16):
            kraus_operators[i,:,:] += (np.sqrt(eigenvals[i])*U[j,i]*Etilde[j,:,:]);
            
    # Calculate the transfer matrix of the channel        
    matrix_rep = transfer_matrix(kraus_operators)
    return matrix_rep                                                    
        
                                                      
                                                      
                                                      
    
                                              
                                                      
                                                      
                                                      
                                                      
    











