# Copyright 2021 Tamara Kohler, Emilio Onorati and Toby Cubitt

#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at

#        http://www.apache.org/licenses/LICENSE-2.0

#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.



import numpy as np
import scipy.linalg as la
import cvxpy as cp
from sympy import Matrix
import scipy.optimize as op
import math
import sys
from operator import mul    
from fractions import Fraction
from functools import reduce



#%%

#construct flip operator on d-dimensional space

def Flip_operator(d):

    F = np.zeros((d,d))
    
    qd =int(np.sqrt(d))
    
    for a in range(qd):
        for b in range(qd):
            F[a+b*qd,b+a*qd]=1
    
    return F



#%%
#Choi_Jamiolkowski transformation of a d^2xd^2 matrix
def Choi(M):
    
    d = int((M.size)**(1/4))
    
    N = np.zeros((d**2,d**2),dtype=complex)
    
    for x in range(d):
        for y in range(d):
            for z in range(d):
                for w in range(d):
                    
                    N[x*d+y,z*d+w] = M[x*d+z,y*d+w]
                    
    return N





#%%
# Check if channel is ND2
    
def checkND2(M):
    
    ma = Matrix(M)
    
    if not ma.is_diagonalizable:
        return False
    
    if len(np.unique(la.eigvals(M))) != M.size**0.5:
        return False
    
    else:
        return True
    
    
#%%
#  Calculate nCk

def nCk(n,k): 
  return int( reduce(mul, (Fraction(n-i, i+1) for i in range(k)), 1) )


#%%
  
def find_in_list_of_list(mylist, char):
    for sub_list in mylist:
        if char in sub_list:
            return [mylist.index(sub_list), sub_list.index(char)]
    raise ValueError("'{char}' is not in list".format(char = char))


#%%
# Function to create a set from a list but keeping order in which elements first appear in list


def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


#%%
# Given a set of pairs of indices, and an index x, find all other elements which are in a pair with x    

    
def all_degenerate_elements(element,degenerate_pairs):
    element_position = np.asarray(degenerate_pairs==element).nonzero()
    degenerate_set = [element]
    for j in range(np.shape(element_position)[1]):
        pairs_position = element_position[0][j],(element_position[1][j]+1)%2
        degenerate_set.append(degenerate_pairs[pairs_position])
            
    return degenerate_set

#%%
# Given a set of pairs of indices (representing degenerate eigenvalues), construct sets of indices representing eigenvalues which are mutually degenerate   

    
def construct_degenerate_sets(degenerate_pairs):
    sets = []
    for i in range(np.shape(degenerate_pairs)[0]):
        initial_element = degenerate_pairs[i,0]
    
        if not any(initial_element in sl for sl in sets):
            degenerate_elements = all_degenerate_elements(initial_element,degenerate_pairs)
        
            degenerate_set = unique(degenerate_elements)
            k=1
            while k < len(degenerate_set):
                kth_element = degenerate_set[k]
                kth_degenerate_elements = all_degenerate_elements(kth_element,degenerate_pairs)
                degenerate_set.extend(kth_degenerate_elements)
                degenerate_set = unique(degenerate_set)
                k+=1
    
            sets.append(degenerate_set)
            
    return sets
    

#%% 
# Function to construct hermitian-conjugate bases for degenerate eigenvalues (complex or real negative)
  
def construct_complex_basis(Mdata,degenerate_set,conjugate_degenerate_set):
    
    dim = int((Mdata.size)**(1/2))
    F = Flip_operator(dim)
    eigvecs = la.eig(Mdata)[1]
    
    deg_size = len(degenerate_set)
                    
    
    deg_eigvecs = np.asarray([eigvecs[:,degenerate_set[i]] for i in range(deg_size)])
    conj_deg_eigvecs = [eigvecs[:,conjugate_degenerate_set[i]] for i in range(deg_size)]
    
    A1 = [F@(deg_eigvecs[i].conj()) for i in range(deg_size)]
    A2 = [-conj_deg_eigvecs[i] for i in range(deg_size)]
    
    
    A1.extend(A2)
    A = np.asarray(A1).T
    KernelA = la.null_space(A)
    
    nullity = KernelA.shape[1]
    
    if nullity == deg_size:
        new_eigvec_basis = np.zeros((dim,deg_size),complex)
        for i in range(deg_size):
            values_to_sum = [KernelA[i,j].conj()*deg_eigvecs[j,:] for j in range(deg_size)]
            new_eigvec_basis[:,i] = np.sum(values_to_sum,axis=0)
            
        return True, new_eigvec_basis
    else:
        return False,[]
    




#%% 
# Function to construct self adjoint & hermitian-conjugate bases for degenerate eigenvalues (real positive)


def construct_positive_basis(Mdata,degenerate_set,precision):
    
    dim = int((Mdata.size)**(1/2))
    F = Flip_operator(dim)
    eigvecs = la.eig(Mdata)[1]
    
    deg_size = len(degenerate_set)
                    
    
    deg_eigvecs = np.asarray([eigvecs[:,degenerate_set[i]] for i in range(deg_size)])
    
    A1 = [F@(deg_eigvecs[i].conj()) for i in range(deg_size)]
    A2 = [-deg_eigvecs[i] for i in range(deg_size)]
    
    A1.extend(A2)
    A = np.asarray(A1).T
    KernelA = la.null_space(A)
    
    nullity = KernelA.shape[1]
    
    if nullity == deg_size:
        check_if_self_adjoint = [all([(la.norm(KernelA[j,i]-KernelA[j+deg_size,i])<precision) for j in range(deg_size)])for i in range(deg_size)]
        number_of_self_adjoint_vectors = sum(check_if_self_adjoint)
        number_of_hc_vecs = deg_size - number_of_self_adjoint_vectors
        if number_of_hc_vecs%2 != 0:
            check_how_non_self_adjoint = np.asarray([sum([la.norm(KernelA[j,i]-KernelA[j+deg_size,i]) for j in range(deg_size)])for i in range(deg_size)])
            assume_is_self_adjoint = np.partition(check_how_non_self_adjoint, number_of_self_adjoint_vectors)[number_of_self_adjoint_vectors]
            additional_self_adjoint_index = np.where(check_how_non_self_adjoint ==assume_is_self_adjoint)[0][0]
            check_if_self_adjoint[additional_self_adjoint_index]=True
            number_of_self_adjoint_vectors+=1
            number_of_hc_vecs-=1
            
        self_adjoint_vecs = np.zeros((dim,number_of_self_adjoint_vectors),complex)
        hc_vecs = np.zeros((dim,number_of_hc_vecs),complex)
        j=0
        k=0
        for i in range(deg_size):
            values_to_sum = [KernelA[i,j].conj()*deg_eigvecs[j,:] for j in range(deg_size)]
            if check_if_self_adjoint[i]:
                self_adjoint_vecs[:,j] = np.sum(values_to_sum,axis=0)
                j+=1
            else:   
                hc_vecs[:,k] = np.sum(values_to_sum,axis=0)
                k+=1
                
        return True, [self_adjoint_vecs,hc_vecs]
    
    else:
        return False,[]
    
    
    
#%% Function to determine which eigenvectors are self adjoint, and which are conjugate pairs
        
def pair_up_eigenvectors(Mdata, positive_sets,positive_bases,negative_sets,complex_sets, conjugate_pairs_of_eigs,precision):
    
    dim = int((Mdata.size)**(1/2))
    eigenvalues = la.eig(Mdata)[0]
    
    self_adjoint_indices = []
    conjugate_pairs_of_indices = []
    
    for sl in positive_sets:
        sl.sort()
        
    for sl in negative_sets:
        sl.sort()
        
    for sl in complex_sets:
        sl.sort
        
    all_degenerate = []
    all_degenerate.extend(positive_sets)
    all_degenerate.extend(negative_sets)
    all_degenerate.extend(complex_sets)
    
    for i in range(dim):
        if any(i in pair for pair in conjugate_pairs_of_indices):
            continue
        if not any(i in sl for sl in all_degenerate):
            if (eigenvalues[i].imag)<precision:
                self_adjoint_indices.extend([i])
            else: 
                conjugate_eig = np.round(eigenvalues[i].conj(),int(np.log10(1/precision)))
                index_of_conj_eig = np.where(np.round(eigenvalues,int(np.log10(1/precision)))==conjugate_eig)[0][0]
                if index_of_conj_eig == i:
                    self_adjoint_indices.extend([i])
                else:                    
                    conjugate_pairs_of_indices.extend([[i,index_of_conj_eig]])
                
        elif any(i in sl for sl in positive_sets):
            indices = find_in_list_of_list(positive_sets,i)
            which_deg_set = indices[0]
            which_deg_element = indices[1]
            size_of_deg_set = np.shape(positive_sets[which_deg_set])[0]
            constructed_basis = positive_bases[which_deg_set]

            self_adjoint_bases = constructed_basis[0]
            
            number_SA_bases = np.shape(self_adjoint_bases)[1]
            number_HC_bases = size_of_deg_set - number_SA_bases
            
            if which_deg_element < number_SA_bases:
                self_adjoint_indices.extend([i])
                
            elif which_deg_element < (number_SA_bases + (number_HC_bases / 2)):
                j = int(which_deg_element + (number_HC_bases / 2))
                index_of_conj_eig = positive_sets[which_deg_set][j]
                conjugate_pairs_of_indices.extend([[i,index_of_conj_eig]])
                
        
                
        elif any(i in sl for sl in negative_sets):
            indices = find_in_list_of_list(negative_sets,i)
            which_deg_set = indices[0]
            which_deg_element = indices[1]
            size_of_deg_set = np.shape(negative_sets[which_deg_set])[0]        
            
            j = int(which_deg_element - (size_of_deg_set / 2))
            index_of_conj_eig = negative_sets[which_deg_set][j]
            conjugate_pairs_of_indices.extend([[i,index_of_conj_eig]])
            
        elif any(i in sl for sl in complex_sets):
            indices = find_in_list_of_list(complex_sets,i)
            which_deg_set = indices[0]
            which_deg_element = indices[1]
            
            which_pair_of_conjugate_eigs= np.where(conjugate_pairs_of_eigs==which_deg_set)[0][0]
            
            if np.where(conjugate_pairs_of_eigs==which_deg_set)[1] == 1:
                conj = 0
            else: 
                conj = 1
                
            conjugate_set = int(conjugate_pairs_of_eigs[which_pair_of_conjugate_eigs,conj])
            
            index_of_conj_eig = complex_sets[conjugate_set][which_deg_element]
            conjugate_pairs_of_indices.extend([[i,index_of_conj_eig]])
   
    return self_adjoint_indices, conjugate_pairs_of_indices
    
#%%
# Function to construct a basis for case of degenerate eigenvalues
    
def construct_random_basis(Mdata,positive_sets,positive_bases,negative_sets,negative_bases,complex_sets,complex_bases, conjugate_pairs_of_eigs):
    
    dim = int((Mdata.size)**(1/2))
    F = Flip_operator(dim)
    eigvecs = la.eig(Mdata)[1]
    
    tilde_basis = np.zeros((dim,dim),complex)
    basis = np.zeros((dim,dim),complex)
    
    for sl in positive_sets:
        sl.sort()
        
    for sl in negative_sets:
        sl.sort()
        
    for sl in complex_sets:
        sl.sort
    
    all_degenerate = []
    all_degenerate.extend(positive_sets)
    all_degenerate.extend(negative_sets)
    all_degenerate.extend(complex_sets)
    
    values_to_repeat_at_end = []
    
    for i in range(dim):
        if not any(i in sl for sl in all_degenerate):
            basis[:,i] = eigvecs[:,i]
        elif any(i in sl for sl in positive_sets):
            indices = find_in_list_of_list(positive_sets,i)
            which_deg_set = indices[0]
            which_deg_element = indices[1]
            size_of_deg_set = np.shape(positive_sets[which_deg_set])[0]
            constructed_basis = positive_bases[which_deg_set]
            
            self_adjoint_bases = constructed_basis[0]
            herm_conj_bases = constructed_basis[1]
            
            number_SA_bases = np.shape(self_adjoint_bases)[1]
            number_HC_bases = size_of_deg_set - number_SA_bases
            
            if which_deg_element < number_SA_bases:
                random_seeds = np.random.rand(number_SA_bases)
                tilde_basis[:,i] = sum([random_seeds[j]* self_adjoint_bases[:,j] for j in range(number_SA_bases)])
                basis[:,i] = tilde_basis[:,i] / la.norm(tilde_basis[:,i])
            
            elif which_deg_element < (number_SA_bases + (number_HC_bases / 2)):
                random_exponents = np.exp(2j*np.pi*np.random.rand(number_HC_bases))
                random_seeds = random_exponents*np.random.rand(number_HC_bases)
                tilde_basis[:,i] = sum([random_seeds[j]* herm_conj_bases[:,j] for j in range(number_HC_bases)])
                basis[:,i] = tilde_basis[:,i] / la.norm(tilde_basis[:,i])
                
            else:
                j = int(which_deg_element - (number_HC_bases / 2))
                conjugate_value = positive_sets[which_deg_set][j]
                tilde_basis[:,i] = F@basis[:,conjugate_value].conj()
                basis[:,i] = tilde_basis[:,i] / la.norm(tilde_basis[:,i])
                        
        elif any(i in sl for sl in negative_sets):
            indices = find_in_list_of_list(negative_sets,i)
            which_deg_set = indices[0]
            which_deg_element = indices[1]
            size_of_deg_set = np.shape(negative_sets[which_deg_set])[0]            
            constructed_basis = negative_bases[which_deg_set]
            
            if which_deg_element < (size_of_deg_set / 2):
                random_exponents = np.exp(2j*np.pi*np.random.rand(size_of_deg_set))
                random_seeds = random_exponents*np.random.rand(size_of_deg_set)
                tilde_basis[:,i] = sum([random_seeds[j]* constructed_basis[:,j] for j in range(size_of_deg_set)])
                basis[:,i] = tilde_basis[:,i] / la.norm(tilde_basis[:,i])
                
            else:
                j = int(which_deg_element - (size_of_deg_set / 2))
                conjugate_value = negative_sets[which_deg_set][j]
                tilde_basis[:,i] = F@basis[:,conjugate_value].conj()
                basis[:,i] = tilde_basis[:,i] / la.norm(tilde_basis[:,i])
                
        elif any(i in sl for sl in complex_sets):
            indices = find_in_list_of_list(complex_sets,i)
            which_deg_set = indices[0]
            which_deg_element = indices[1]
            size_of_deg_set = np.shape(complex_sets[which_deg_set])[0]
            
            which_pair_of_conjugate_eigs= np.where(conjugate_pairs_of_eigs==which_deg_set)[0][0]
            constructed_basis = complex_bases[which_pair_of_conjugate_eigs]
            
            if np.where(conjugate_pairs_of_eigs==which_deg_set)[1] == 1:
                conj = True
            else: 
                conj = False
            
            random_exponents = np.exp(2j*np.pi*np.random.rand(size_of_deg_set))
            random_seeds = random_exponents*np.random.rand(size_of_deg_set)
            
            if not conj:
                tilde_basis[:,i] = sum([random_seeds[j]* constructed_basis[:,j] for j in range(size_of_deg_set)])
                basis[:,i] = tilde_basis[:,i] / la.norm(tilde_basis[:,i])
            if conj:
                conjugate_set = int(conjugate_pairs_of_eigs[which_pair_of_conjugate_eigs,0])
                conjugate_value = complex_sets[conjugate_set][which_deg_element]
                if conjugate_value < i:
                    tilde_basis[:,i] = F@basis[:,conjugate_value].conj()
                    basis[:,i] = tilde_basis[:,i] / la.norm(tilde_basis[:,i])
                    
                else: 
                    need_to_repeat = [i,conjugate_value]
                    values_to_repeat_at_end.append(need_to_repeat)
                
    for i in range(len(values_to_repeat_at_end)):
        need_to_repeat = values_to_repeat_at_end[i]
        value = need_to_repeat[0]
        conjugate_value = need_to_repeat[1]
        
        tilde_basis[:,value] = F@basis[:,conjugate_value].conj()
        basis[:,value] = tilde_basis[:,value] / la.norm(tilde_basis[:,value])
                
    return basis

#%%    
    
# Convex optimisation algorithm to search for the closest Lindbladian to the matrix logarithm

def search_LB(L):
    
    dim = np.rint((L.size)**(1/2)).astype(int)
    onesysdim = np.rint(np.sqrt(dim)).astype(int)
    Id_onesys= np.eye(onesysdim)
    #projector to the complement space of the maximally entangled state
    
    max_ent_state = sum([np.kron(Id_onesys[j],Id_onesys[j]) for j in range(onesysdim)])
    P_wcomp = np.eye(dim)-np.outer(max_ent_state,max_ent_state)/np.sqrt(dim)
    
    
    #convex-optimization program
    
    X = cp.Variable((dim,dim), hermitian=True)
    XTP = sum( np.kron(Id_onesys[j],Id_onesys) @ X @ (np.kron(Id_onesys[j],Id_onesys).T) for j in range(onesysdim) )
    
    obj = cp.Minimize(cp.norm(X-Choi(L),'fro'))
    constraint = [P_wcomp@X@P_wcomp>>0, cp.pnorm(XTP,1)<=10**(-8)] #tolerance of constraint may be turned into variable
    
    prob = cp.Problem(obj, constraint)
    prob.solve()
    
    if X.value is not None:
        L2 = Choi(X.value)
        return L2
    
    else:
        return np.zeros(1)
    
    
#%% # Functino to find Markovian channel in epsilon neighbourhood (if it exists)
        
def findLindblad(Madjusted, Mdata, epsilon, dI,self_adjoint_indices,conjugate_pairs_indices,m=1):
    
    dim = int((Madjusted.size)**(1/2))

    #define the principal branch
    L0 = la.logm(Madjusted,disp=False)[0]
    
    #set up projectors
    R_eigvect = np.linalg.eig(L0)[1]
    L_eigvect = la.inv(R_eigvect)
    
    projectors = [np.outer(R_eigvect[:,index],(L_eigvect[index])) for index in range(dim)]
    
    
        
# Run convex optimsation to find closest Lindbladian consistent with channel
    #set variables
    mindex = np.zeros(dim)
    d = dI
    Lm = np.zeros( (dim,dim) )
    
    branching_number = ((2*m)+1)**len(conjugate_pairs_indices)
        
    first_of_pair = np.zeros(len(conjugate_pairs_indices),int)
    second_of_pair = np.zeros(len(conjugate_pairs_indices),int)
    
    k = 0
        
    for sl in conjugate_pairs_indices:
        first_of_pair[k] = sl[0]
        second_of_pair[k] = sl[1]
        mindex[first_of_pair[k]] = -m
        mindex[second_of_pair[k]] = +m
        k+=1
        
        
    #run the loop
    for n in range(branching_number):
                
        #create the branch of the logarithm
        Lm = L0 + 2j*np.pi*sum([mindex[ind]*projectors[ind] for ind in range(dim)])
        
        Lindblad = search_LB(Lm)
        
        if np.any(np.isnan(la.expm(Lindblad))) or np.any(np.isinf(la.expm(Lindblad))): continue
    
        distance = la.norm(Mdata - la.expm(Lindblad) ,'fro' )
        
        if distance < d:
            d = distance
            Lprime = Lindblad
                
        #set the next branch index
        if branching_number>1:
            i=0    
            while mindex[first_of_pair[i]]==m and n < (branching_number-1):
                mindex[first_of_pair[i]]=-m
                mindex[second_of_pair[i]]=m
                i+=1
            mindex[first_of_pair[i]]=mindex[first_of_pair[i]]+1
            mindex[second_of_pair[i]]=mindex[second_of_pair[i]]-1
            
    # Check if channel corresponding to Lindbladian is in epsilon neighbourhood of M
    if d < epsilon:
        return Lprime, d, 0
    else:
        return np.zeros(1), dI, 1
    




             
#%%                 
# Retrieve smallest value of non-Markovianity parameter mu
    
def nonMarkParameter(Madjusted,Mdata,conjugate_pairs_indices,epsilon=1,muI = 10**3,m=1,delta_steps = 10**(-5)):
    
    dim = int((Mdata.size)**(1/2))
      
    # Set minimum delta (norm distance allowed on logarithms) using Lemma 11
    L0 = la.logm(Madjusted,disp=False)[0]
    normL = la.norm(L0,'fro')
    def func(x):
        return x*math.exp(x) - epsilon/normL
    delta_min = op.newton(func,1)
    
    
    # Set maximum delta (norm distance allowed on logarithms) using Corollary 15
    #c = 1
    #k = min(1,(134**2*(1+c)**2*epsilon**2*la.norm(M,'fro')**2)**0.5)
    #delta_max = 134*k*(1+c**2)*(1+(k*la.norm(M,'fro'))+(k*epsilon*(1+(k**2*la.norm(M,'fro')**2))**0.5))*epsilon
    # (This is too large - bound isn't tight, use C*delta_min instead)
    delta_max = 100*delta_min
    
    #projector to the complement space of the maximally entangled state
    
    max_ent_state = sum([np.kron(np.eye(int(np.round(np.sqrt(dim))))[j],np.eye(int(np.round(np.sqrt(dim))))[j]) for j in range(int(np.rint(np.sqrt(dim))))])
    P_wcomp = np.eye(dim)-np.outer(max_ent_state,max_ent_state)/np.sqrt(dim)
    
    
    #set up projectors
    R_eigvect = np.linalg.eig(L0)[1]
    L_eigvect = la.inv(R_eigvect)
    
    projectors = [np.outer(R_eigvect[:,index],(L_eigvect[index])) for index in range(dim)]
    
    
    #Loop over branches of the logarithm 
    mu_h = muI
    
    delta = delta_min
    
    
    while delta < delta_max:
        m_h = np.zeros(dim)
        
        branching_number = ((2*m)+1)**len(conjugate_pairs_indices)
        
        first_of_pair = np.zeros(len(conjugate_pairs_indices),int)
        second_of_pair = np.zeros(len(conjugate_pairs_indices),int)
    
        k = 0
    
        for sl in conjugate_pairs_indices:
            first_of_pair[k] = sl[0]
            second_of_pair[k] = sl[1]
            m_h[first_of_pair[k]] = -m
            m_h[second_of_pair[k]] = +m
            k+=1
            

        for index in range(branching_number):
            Lm = L0 + (2j*np.pi)*sum([m_h[ind]*(projectors[ind]) for ind in range(dim)])

            # Convex optimisation program
            X = cp.Variable((dim,dim),hermitian=True)
            mu = cp.Variable(1,nonneg=True)
            obj = cp.Minimize(mu)
            
            onesysdim = np.rint(np.sqrt(dim)).astype(int)
            Id_onesys= np.eye(onesysdim)
            XTP = sum( np.kron(Id_onesys[j],Id_onesys) @ X @ (np.kron(Id_onesys[j],Id_onesys).T) for j in range(onesysdim) )

            constraint = [mu>=0,cp.norm(X - Choi(Lm),'fro')<=delta,P_wcomp@X@P_wcomp+(mu/np.sqrt(dim)*np.eye(dim))>>0 ,cp.pnorm(XTP,1)<=10**(-8)]

            
            prob = cp.Problem(obj, constraint)
            prob.solve(solver=cp.SCS)
            
            
            
            if X.value is not None:
                L2 = Choi(X.value)     

                if la.norm(la.expm(L2)-Mdata,'fro')<epsilon:
                    if mu.value is not None:
                        if mu.value < mu_h:
                            mu_h= mu.value
                                        
                
            if branching_number>1:
                i=0    
                while m_h[first_of_pair[i]]==m and index < branching_number-1:
                    m_h[first_of_pair[i]]=-m
                    m_h[second_of_pair[i]]=m
                    i+=1
                m_h[first_of_pair[i]]=m_h[first_of_pair[i]]+1
                m_h[second_of_pair[i]]=m_h[second_of_pair[i]]-1

        delta += delta_steps

        
    return mu_h




#%% 
# (For particular choice of basis if degenerate = true): Retrieve closest Lindlbadian if one exists in epsilon neighbourhood, if not calculate non Markovianity parameter         
        
def checkMarkov(Mdata,self_adjoint_indices,conjugate_pairs_indices,epsilon = 1,basis_set = np.zeros(1),degenerate = False,find_mu = True,dI = 10**3,m=1):
    
    
    dim = int((Mdata.size)**(1/2))
    F = Flip_operator(dim)
        
    # Check that Choi anti Hermitian part of Mdata is less than epsilon
    choiAntiHermM = 0.5*(Mdata - (F@Mdata.conj()@F))
    if la.norm(choiAntiHermM,'fro') > epsilon:
        return np.zeros(1), dI, 2
        
    elif not degenerate:

        # If Mdata is ND2 run checkMark. If Lindblad found return Lindblad, otherwise calculate mu. If M is not ND2 return that
        if checkND2(Mdata): 
            Lprime, d, test = findLindblad(Mdata,Mdata,epsilon,dI,self_adjoint_indices,conjugate_pairs_indices,m)
            if test == 0:
                return Lprime, d, test
            elif find_mu:
                mu = nonMarkParameter(Mdata,Mdata,conjugate_pairs_indices,epsilon,dI,m)
                return np.zeros(1), mu, test
            else:
                return np.zeros(1),dI,test
        else:
             return np.zeros(1), dI, 3
    
    
    else:
        
        Mdataval = la.eigvals(Mdata)
        Madjusted = basis_set@np.diag(Mdataval)@la.inv(basis_set)
        

        
        # Check Madjusted is ND2 
        if checkND2(Madjusted):
            Lprime, d, test = findLindblad(Madjusted,Mdata,epsilon,dI,self_adjoint_indices,conjugate_pairs_indices,m)

            if test == 0:
                return Lprime, d, test
            elif find_mu:
                mu = nonMarkParameter(Madjusted,Mdata,conjugate_pairs_indices,epsilon,dI,m)
                return np.zeros(1), mu, test
            else:
                return np.zeros(1),dI,test
                 
        else:
            return np.zeros(1),dI, 3
    
        
#%%
# Function to test Markovianity on random bases
            
def testRandomBases(Mdata, bases, sets, epsilon, dI, random_samples, conjugate_pairs_of_eigs,precision, finding_mu,m=1):
    
    dim = int((Mdata.size)**(1/2))
    
    Lindblad = np.zeros((dim,dim))
    distance_check = dI
    test_check = 3
    mu_check = dI
        
    positive_sets = sets[0]
    negative_sets = sets[1]
    complex_sets = sets[2]
    
    positive_bases = bases[0]
    negative_bases = bases[1]
    complex_bases = bases[2]
    
    self_adjoint_indices,conjugate_pairs_indices = pair_up_eigenvectors(Mdata, positive_sets,positive_bases,negative_sets,complex_sets, conjugate_pairs_of_eigs,precision)

    
    for sample in range(random_samples):
        
    
        basis = construct_random_basis(Mdata,positive_sets,positive_bases,negative_sets,negative_bases,complex_sets,complex_bases, conjugate_pairs_of_eigs)
                
        if np.linalg.cond(basis) > (1/sys.float_info.epsilon): 
            continue
        
        if finding_mu:
            find_mu = test_check > 0
        else:
            find_mu = False 
         
                
        Lprime, mu_distance, test = checkMarkov(Mdata,self_adjoint_indices,conjugate_pairs_indices,epsilon,basis,True,find_mu,dI,m)
                
        if test < test_check:
            test_check = test
                
        if test == 0:
            if mu_distance<distance_check:
                distance_check = mu_distance
                Lindblad = Lprime
            
        if test == 1:   
            if mu_distance < mu_check:
                mu_check = mu_distance
            
        if test == 2:
            break
        
        
    return Lindblad, mu_check,distance_check, test_check


#%%

# Main algorithm (including preprocessing to deal with degeneracy) - given tomographic snapshot & parameters find Lindbladian (if one exists in epsilon neighbourhood), or compute non-Markovianity parameter

def MarkovianityTest(Mdata,epsilon=1,random_samples = 100, precision = 10**(-2),dI = 10**3, finding_mu = True,m=1):
    
    dim = int((Mdata.size)**(1/2))
    
    
    # Check how many pairs of real degenerate eigenvalues there are
    eigenvalues = la.eigvals(Mdata)
    checklist_complex = [((la.norm(eigenvalues[i]-eigenvalues[j]) < precision) and ((la.norm(eigenvalues[i].imag))>precision)) for i in range(dim) for j in range(dim) if i>j]
    checklist_positive = [((la.norm(eigenvalues[i]-eigenvalues[j]) < precision) and ((la.norm(eigenvalues[i].imag))<precision) and (eigenvalues[i].real>0)) for i in range(dim) for j in range(dim) if i>j]
    checklist_negative = [((la.norm(eigenvalues[i]-eigenvalues[j]) < precision) and ((la.norm(eigenvalues[i].imag))<precision) and (eigenvalues[i].real<0)) for i in range(dim) for j in range(dim) if i>j]

    checklist = []
    checklist.extend(checklist_complex)
    checklist.extend(checklist_positive)
    checklist.extend(checklist_negative)
    
    
    if sum(checklist) == 0:
        self_adjoint,conjugate_pairs = pair_up_eigenvectors(Mdata, [],[[],[]],[],[], [],precision)


        Lindblad, mu_distance, test = checkMarkov(Mdata,self_adjoint,conjugate_pairs,epsilon,np.zeros(1),False,finding_mu,dI,m)
        if test==0:
            distance = mu_distance
        if test==1:
            mu = mu_distance
          
    elif sum(checklist_positive) == nCk(dim,2):
        return 'Channel is consistent with the identity map', np.zeros(1), 0
    
    else:
        #List of all possible pairs of Hermitian conjugate eigenvectors
        pairs_list = [[j,i] for i in range(dim) for j in range(dim) if i>j]

    
        #Arrays of which pairs have same eigenvalues up to precision
        degenerate_pairs_complex = np.asarray([pairs_list[i] for i in range(nCk(dim,2)) if checklist_complex[i]])
        degenerate_pairs_positive = np.asarray([pairs_list[i] for i in range(nCk(dim,2)) if checklist_positive[i]])
        degenerate_pairs_negative = np.asarray([pairs_list[i] for i in range(nCk(dim,2)) if checklist_negative[i]])
        
        
        # Construct degenerate sets
        positive_degenerate_sets = construct_degenerate_sets(degenerate_pairs_positive)
        negative_degenerate_sets = construct_degenerate_sets(degenerate_pairs_negative)
        complex_degenerate_sets = construct_degenerate_sets(degenerate_pairs_complex)



        # Check that every degenerate complex eigenvalue has a conjugate eigenvalue with equal degeneracy
        complex_deg_counter = len(complex_degenerate_sets)
        
        
        if complex_deg_counter%2 != 0:
            raise NameError("There are complex degenerate eigenvalues with no conjugate")
        
        else:
            complex_deg_pairs = int(complex_deg_counter / 2)
            conjugate_pairs_of_eigs = np.zeros((complex_deg_pairs,2))
            x=0
            for i in range(complex_deg_counter):
                if i!=0 and i in conjugate_pairs_of_eigs:
                    continue
                
                complex_set = complex_degenerate_sets[i]
                deg_eigenvalue = eigenvalues[complex_set[0]]
                
                conjugate_eig = np.round(deg_eigenvalue.conj(),int(np.log10(1/precision)))
                
                index_of_conj_eig = np.where(np.round(eigenvalues,int(np.log10(1/precision)))==conjugate_eig)
                index_of_conj_set = np.where(np.asarray(complex_degenerate_sets)==index_of_conj_eig)[0][0]
                
                conjugate_set = complex_degenerate_sets[index_of_conj_set]
                
                if len(conjugate_set)!=len(complex_set):
                    raise NameError("There are complex degenerate eigenvalues with no conjugate")

                conjugate_pairs_of_eigs[x,0] = i
                conjugate_pairs_of_eigs[x,1] = index_of_conj_set
                x+=1
        
        
        
        # Construct bases for degenerate eigenvectors which are conjugate / self adjoint as required
        positive_bases = []
        negative_bases = []
        complex_bases = []
    
        
        for i in range(len(positive_degenerate_sets)):
            test_basis,constructed_basis = construct_positive_basis(Mdata,positive_degenerate_sets[i],precision)
            if test_basis:
                positive_bases.append(constructed_basis)
            else:
                raise NameError('There is no solution to the Kernel equation')

        for i in range(len(negative_degenerate_sets)):
            if len(negative_degenerate_sets[i])%2 !=0:
                raise NameError('There are negative eigenvalues with odd degeneracy')
            test_basis,constructed_basis = construct_complex_basis(Mdata,negative_degenerate_sets[i],negative_degenerate_sets[i])
            if test_basis:
                negative_bases.append(constructed_basis)
            else:   
                raise NameError('There is no solution to the Kernel equation')
            
        for i in range(complex_deg_pairs):
            complex_set = complex_degenerate_sets[int(conjugate_pairs_of_eigs[i,0])]
            conj_set = complex_degenerate_sets[int(conjugate_pairs_of_eigs[i,1])]
            test_basis,constructed_basis = construct_complex_basis(Mdata,complex_set,conj_set)
            if test_basis:
                complex_bases.append(constructed_basis)
            else:   
                raise NameError('There is no solution to the Kernel equation')
       
        bases = []
        bases.append(positive_bases)
        bases.append(negative_bases)
        bases.append(complex_bases)
        
        sets= []
        sets.append(positive_degenerate_sets)
        sets.append(negative_degenerate_sets)
        sets.append(complex_degenerate_sets)
        
        Lindblad, mu,distance,test = testRandomBases(Mdata, bases, sets,epsilon, dI, random_samples, conjugate_pairs_of_eigs,precision,finding_mu,m)
                
                            

    if test == 0:
        return 'Lindbladian found', Lindblad, distance
                
    if test==1:
        return 'Non-markovianity parameter', np.zeros(1), mu
                
    if test==2:
        return 'No Markovian channel or non-markovianity parameter within epsilon neighbourhood', np.zeros(1), 0
                
    if test==3:
        return 'No ND2 inputs given', np.zeros(1), 0
            
                    
          





