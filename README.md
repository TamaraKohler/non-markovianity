Package to find Lindbladians / non-Markovianity parameters from tomographic snapshots.

The process_tomography.py file contains algorithms for carrying out 1- and 2-qubit process tomography in Cirq.

The testing-markovianity.py file contains code for finding Lindbladians / non-Markovianity parameters from tomographic snapshots. The main algorithm is:

MarkovianityTest(Mdata,epsilon=1,random_samples = 100, precision = 10**(-2),dI = 10**3, finding_mu = True)

The inputs are:
- Mdata (the tomographic snapshot)
- epsilon (error tolerance parameter)
- random samples (number of random bases to check in case of degenerate eigenvalues)
- precision (to use when checking if eigenvalues are degenerate)
- dI (large value - to be used as initial `worst case' when checking distances)
- finding_mu (Boolean value - true if you want to find non-Markovianity parameter, false if you just want to find best fit Lindbladian - running the code with finding_mu = True is significantly slower than with finding_mu = False)

There are three outputs:
- Result - whether a Lindbladian was found or not
- Lindbladian - if a Lindbladian was found, then the Lindbladian, else np.zeros(1)
- Distance - if a Lindbladian was found then the distance between exp(Lindbladian) and the tomographic snapshot, else the non-Markovianity parameter, mu